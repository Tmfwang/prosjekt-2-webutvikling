import React from "react";
import "./Styles/MainPage.css";
import "./Styles/IndexStyle.css";
import CategorySelectorContainer from "./components/CategorySelectorContainer";
import TabContainer from "./components/TabContainer";
import Header from "./components/Header";
import Footer from "./components/Footer";
import GalleryContainer from "./components/GalleryContainer";

class App extends React.Component {
  state = {
    //Contains info about which is tab is currently active
    tabTypes: [
      {
        id: 0,
        name: "Bilde 1",
        currentTab: 0
      },

      {
        id: 1,
        name: "Bilde 2",
        currentTab: 0
      },

      {
        id: 2,
        name: "Bilde 3",
        currentTab: 0
      },

      {
        id: 3,
        name: "Bilde 4",
        currentTab: 0
      }
    ],
    //Contains information about the different animals and which category is chosen
    categoryTypes: [
      {
        id: 0,
        name: "Bilder",
        categories: ["Dyr", "Verdensrom", "Byer"],
        chosen: 0
      },
      {
        id: 1,
        name: "Tekster",
        categories: ["Dikt", "Sanger", "Sitat"],
        chosen: 0
      },
      {
        id: 2,
        name: "Lyder",
        categories: ["Dyr", "Vann", "Maskiner"],
        chosen: 0
      }
    ],
    //Contains urls for all mediatypes
    images: [
      {
        categoryId: 0,
        name: "nature",
        urls: [
          "/svg/dog.svg",
          "/svg/elephant.svg",
          "/svg/tortoise.svg",
          "/svg/zebra.svg"
        ]
      },
      {
        categoryId: 1,
        name: "space",
        urls: [
          "/svg/fullmoon.svg",
          "/svg/spaceSuit.svg",
          "/svg/sun.svg",
          "/svg/rocket.svg"
        ]
      },
      {
        categoryId: 2,
        name: "cities",
        urls: [
          "/svg/London.svg",
          "/svg/LosAngeles.svg",
          "/svg/NewYork.svg",
          "/svg/Philadelphia.svg"
        ]
      }
    ],

    texts: [
      {
        categoryId: 0,
        name: "poems",
        urls: [
          "/text/MaaIkkeSove.json",
          "/text/Silverstein.json",
          "/text/Hughes.json",
          "/text/Teasdale.json"
        ]
      },
      {
        categoryId: 1,
        name: "songs",
        urls: [
          "/text/Metallica.json",
          "/text/Thunderstruck.json",
          "/text/Queen.json",
          "/text/Dragonforce.json"
        ]
      },
      {
        categoryId: 2,
        name: "quotes",
        urls: [
          "/text/Keller.json",
          "/text/Franklin.json",
          "/text/Shakespeare.json",
          "/text/Brown.json"
        ]
      }
    ],

    sounds: [
      {
        categoryId: 0,
        name: "animals",
        urls: [
          "/sounds/barking.mp3",
          "/sounds/meow.mp3",
          "/sounds/rooster.mp3",
          "/sounds/stag.mp3"
        ]
      },
      {
        categoryId: 1,
        name: "water",
        urls: [
          "/sounds/drink.mp3",
          "/sounds/splash.mp3",
          "/sounds/toilet.mp3",
          "/sounds/waterfall.mp3"
        ]
      },
      {
        categoryId: 2,
        name: "vehicles",
        urls: [
          "/sounds/firetruckHorn.mp3",
          "/sounds/foghorn.mp3",
          "/sounds/helicopter.mp3",
          "/sounds/oldCar.mp3"
        ]
      }
    ]
  };

  // Updates the "chosen"-value of the CategoryType-object with the id of categoryId. The value is
  // set to the value of "changedTo".
  categoryTypesChanged = (categoryId, changedTo) => {
    // Sjekker at prevCategories-variabelen eksisterer; hvis ikke: opprett den
    if (sessionStorage.getItem("prevCategories") === null) {
      sessionStorage.prevCategories = JSON.stringify([
        [localStorage.categ0, localStorage.categ1, localStorage.categ2]
      ]);
    }

    // Lagrer tilstanden til kategoriene i en liste i sessionsStorage; slik at det kan undo's etterpaa
    let categoriesList = JSON.parse(sessionStorage.prevCategories);
    categoriesList.push([
      localStorage.categ0,
      localStorage.categ1,
      localStorage.categ2
    ]);
    sessionStorage.prevCategories = JSON.stringify(categoriesList);

    this.setCategoryState(categoryId, changedTo);
  };

  // Funksjon for å gå tilbake til forrige kategori som var valgt
  undoCategory = () => {
    let categoriesList = JSON.parse(sessionStorage.prevCategories);
    let lastCategory = categoriesList.pop();

    // Dersom det faktisk finnes noe som kan undo's
    if (lastCategory) {
      sessionStorage.prevCategories = JSON.stringify(categoriesList);

      // Oppdaterer hvilken kategori som er valgt
      this.setCategoryState(0, Number(lastCategory[0]));
      this.setCategoryState(1, Number(lastCategory[1]));
      this.setCategoryState(2, Number(lastCategory[2]));
    }
  };

  // Funksjon som setter kategori-tilstand
  setCategoryState = (categoryId, changedTo) => {
    this.setState({
      categoryTypes: this.state.categoryTypes.map(categoryType => {
        //endrer localstorage variablen til den nye kategorien, slik at nettleseren husker økten
        if (categoryType.id === categoryId) {
          categoryType.chosen = changedTo;

          if (categoryId === 0) {
            localStorage.categ0 = changedTo;
          }

          if (categoryId === 1) {
            localStorage.categ1 = changedTo;
          }

          if (categoryId === 2) {
            localStorage.categ2 = changedTo;
          }
        }
        return categoryType;
      })
    });
  };

  // Funksjon som kjøres hver gang en bruker velger en ny tab, oppdaterer localstorage variablen, og currenttab state-variablen.
  chooseTab = tabID => {
    localStorage.prevTab = tabID;
    this.setState({
      tabTypes: this.state.tabTypes.map(tab => {
        tab.currentTab = tabID;
        return tab;
      })
    });
  };

  // Returnerer den aktive tab'n
  getActiveTab() {
    // Dersom dette er første gang nettsiden besøkes, så settes aktiv tab til 0
    if (!localStorage.prevTab) {
      this.chooseTab(0);
    }

    // Returnerer den valgte tabben
    return this.state.tabTypes[0].currentTab;
  }

  // Returnerer den valgte bildekategorien
  getImageCategory() {
    return this.state.categoryTypes[0].chosen;
  }

  // Returnerer den valgte tekstkategorien
  getTextCategory() {
    return this.state.categoryTypes[1].chosen;
  }

  // Returnerer den valgte lydkategorien
  getSoundCategory() {
    return this.state.categoryTypes[2].chosen;
  }

  // Funksjon som kjøres ved initiell innlasting av siden; laster inn de lokalt lagrete verdiene
  componentDidMount() {
    // Dersom nettleseren støtter webstorage
    if (typeof Storage !== "undefined") {
      // Dersom den lokale variabelen ikke eksisterer så settes aktiv tab til bilde 1
      if (!localStorage.prevTab) {
        this.chooseTab(0);
      }

      // Dersom brukeren har besøkt siden tidligere, så settes den aktive tab'n lik det brukeren hadde under forrige sesjon
      else {
        this.chooseTab(Number(localStorage.prevTab));
      }

      // Dersom den lokale bildekategori-variabelen ikke eksisterer: velg en tilfeldig bildekategori
      if (!localStorage.categ0) {
        localStorage.categ0 = Math.floor(Math.random() * 3);
      }

      // Dersom den lokale tekstkategori-variabelen ikke eksisterer: velg en tilfeldig tekstkategori
      if (!localStorage.categ1) {
        localStorage.categ1 = Math.floor(Math.random() * 3);
      }

      // Dersom den lokale lydkategori-variabelen ikke eksisterer: velg en tilfeldig lydkategori
      if (!localStorage.categ2) {
        localStorage.categ2 = Math.floor(Math.random() * 3);
      }

      // Oppdaterer tilstanden til kategoriene
      this.categoryTypesChanged(0, Number(localStorage.categ0));
      this.categoryTypesChanged(1, Number(localStorage.categ1));
      this.categoryTypesChanged(2, Number(localStorage.categ2));

      // Setter det første elementet i undo-listen lik nåværende tilstand
      sessionStorage.prevCategories = JSON.stringify([
        [localStorage.categ0, localStorage.categ1, localStorage.categ2]
      ]);
    }
  }

  // Rendrer siden
  render() {
    return (
      <div className="App">
        <Header />
        <div className="MainPageDiv">
          {/* Bildetabs som kun er synlig på desktopversjon */}
          <div id="TabsDesktop">
            <TabContainer
              tabTypes={this.state.tabTypes}
              chooseTab={this.chooseTab}
            ></TabContainer>
          </div>
          <div className="CatAndGallery">
            {/* Beholder for kategoriene */}
            <div className="CatContainer">
              <CategorySelectorContainer
                categoryTypes={this.state.categoryTypes}
                categoryTypesChanged={this.categoryTypesChanged}
              />

              {/* Knapp som undo'er valg av kategorier */}
              <div style={{ textAlign: "center" }}>
                <button id="UndoButton" onClick={this.undoCategory}>
                  Angre Kategorivalg
                </button>
              </div>
            </div>

            {/* Bildetabs som kun er synlig på mobilversjon */}
            <div id="TabsMobile">
              <TabContainer
                tabTypes={this.state.tabTypes}
                chooseTab={this.chooseTab}
              ></TabContainer>
            </div>

            {/* Beholder for galleriet */}
            <div className="GalContainer">
              <GalleryContainer
                image={
                  this.state.images[this.getImageCategory()].urls[
                    this.getActiveTab()
                  ]
                }
                text={
                  this.state.texts[this.getTextCategory()].urls[
                    this.getActiveTab()
                  ]
                }
                sound={
                  this.state.sounds[this.getSoundCategory()].urls[
                    this.getActiveTab()
                  ]
                }
              />
            </div>
          </div>
          <Footer />
        </div>
      </div>
    );
  }
}

export default App;
