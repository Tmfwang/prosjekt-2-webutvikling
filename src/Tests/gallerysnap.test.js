import React from "react";
import GalleryContainer from "./../components/GalleryContainer";
import renderer from "react-test-renderer";
// Snapshot test
it("renders correctly", () => {
  const tree = renderer
    .create(
      <GalleryContainer
        image="/svg/London.svg"
        text="/text/Hughes.json"
        sound="/sounds/stag.mp3"
      ></GalleryContainer>
    )
    .toJSON();
  expect(tree).toMatchSnapshot();
});
