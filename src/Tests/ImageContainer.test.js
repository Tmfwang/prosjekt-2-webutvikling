import waitUntil from "async-wait-until";
import React from "react";
import nock from "nock";
import ReactDOM from "react-dom";
import ImageContainer from "./../components/ImageContainer.js";

describe("testing av ImageContainer", () => {
  beforeEach(() => {
    // Prepare nock to respond to requests in the tests to come.
    nock("http://myapi.com")
      .get("/svg/dog.svg")
      .reply(200, "<svg></svg>", {
        "Access-Control-Allow-Origin": "*",
        "Content-type": "application/json"
      });
  });

  test("renders without crashing", async () => {
    const div = document.createElement("div");
    const imageContainerElement = ReactDOM.render(
      <ImageContainer url="http://myapi.com/svg/dog.svg" />,
      div
    );
    // Wait until the async function in the imageContainerElement is done
    await waitUntil(() => imageContainerElement.state["currentImg"] !== null);
    ReactDOM.unmountComponentAtNode(div);
  });

  test("current_url_updated", async () => {
    const div = document.createElement("div");
    const imageContainerElement = ReactDOM.render(
      <ImageContainer url="http://myapi.com/svg/dog.svg" />,
      div
    );

    expect(imageContainerElement.state["currentImg"]).toEqual(null);

    // Wait until the async function in the imageContainerElement is done
    await waitUntil(() => imageContainerElement.state["currentImg"] !== null);

    // Check that the state is updated correctly
    expect(imageContainerElement.state["currentImg"]).toEqual("<svg></svg>");
    expect(imageContainerElement.state["lastUrl"]).toEqual(
      "http://myapi.com/svg/dog.svg"
    );
    expect(imageContainerElement.state["http://myapi.com/svg/dog.svg"]).toEqual(
      "<svg></svg>"
    );

    ReactDOM.unmountComponentAtNode(div);
  });
});
