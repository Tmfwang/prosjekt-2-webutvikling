import React from "react";
import Header from "./../components/Header";
import renderer from "react-test-renderer";
// Snapshot test
it("renders correctly", () => {
  const tree = renderer.create(<Header></Header>).toJSON();
  expect(tree).toMatchSnapshot();
});
