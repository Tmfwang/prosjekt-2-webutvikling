import React from "react";
import SingleTab from "./SingleTab";
import PropTypes from "prop-types";
import "./../Styles/TabContainerStyle.css";

// En container som inneholder alle de fire tab-knappene
class TabContainer extends React.Component {
  // Funksjon for å generere én tab-knapp
  renderTab() {
    return this.props.tabTypes.map((singleTab, index) => {
      return (
        <SingleTab
          key={index}
          singleTab={singleTab}
          chooseTab={this.props.chooseTab}
        />
      );
    });
  }

  render() {
    return (
      // tabContainer-klassen bruker blant annet flexbox
      <div className="TabContainers">{this.renderTab()}</div>
    );
  }
}

TabContainer.propTypes = {
  tabTypes: PropTypes.array.isRequired
};

export default TabContainer;
