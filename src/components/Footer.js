import React from "react";
import "./../Styles/Footer.css";
//Simple footer locked to the bottom the mainpage.
function Footer() {
  return (
    <footer className="footer-div">
      <p>
        Dette er et prosjekt laget i faget IT-2810 av Tobias Ørstad, Tor Martin
        Wang og Simen Holmestad.
        <br />
        Prosjektet finnes på gitlab med{" "}
        <a
          target="_blank"
          rel="noopener noreferrer"
          href="https://gitlab.stud.idi.ntnu.no/IT2810-H19/teams/team-33/prosjekt-2"
        >
          denne linken
        </a>
        . (Hvis du har tilgang)
      </p>
    </footer>
  );
}

export default Footer;
