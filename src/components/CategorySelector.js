import React from "react";
import PropTypes from "prop-types";
// Contains the controls for changing the active category. Contains a set of radiobuttons.
class CategorySelector extends React.Component {
  // Sends the new value of the radio-buttons to the component above. The value is an integer
  // representing the index of the corresponding string in categories-list.
  radioButtonChanged = e => {
    this.props.handleChange(parseInt(e.target.value));
  };

  // Render the radio-buttons to the DOM. One radio button for each element in the categories-list,
  // i.e "poems", "songs", "quotes". The chosen category is marked as checked. The
  // radiobuttonchanged function is called if the radio-buttons are changed.
  renderRadioButtons() {
    return this.props.categoryType.categories.map((category, index) => {
      return (
        <div key={index} style={{ width: "33%" }}>
          <label>
            <input
              type="radio"
              value={index}
              checked={index === this.props.categoryType.chosen}
              onChange={this.radioButtonChanged}
            />
            {category}
          </label>
        </div>
      );
    });
  }
  //Renders the selector. Places the radiobuttons in a flexbox.
  render() {
    return (
      <div style={{ textAlign: "center" }}>
        <h2>{this.props.categoryType.name}</h2>
        <div style={{ display: "flex", marginBottom: "30px" }}>
          {this.renderRadioButtons()}
        </div>

        <hr></hr>
      </div>
    );
  }
}

CategorySelector.propTypes = {
  categoryType: PropTypes.object.isRequired,
  handleChange: PropTypes.func.isRequired
};

export default CategorySelector;
