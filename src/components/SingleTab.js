import React from "react";
import PropTypes from "prop-types";
import "./../Styles/SingleTabStyle.css";
// Klasse for hvert enkelt faneknapp/tab
class SingleTab extends React.Component {
  // Funksjon som styler tab-knappene; avhengig om tabben er aktiv eller ikke
  getStyle = () => {
    // Når tabben er aktiv, så endres bakgrunnsfargen til tabben
    if (this.props.singleTab.id === this.props.singleTab.currentTab) {
      return {
        backgroundColor: "#707070"
      };
    }

    // Når tabben ikke er aktiv
    return {
      backgroundColor: "#C0C0C0"
    };
  };
  // Renderer en singletab med rett navn inne i en div som er klikkbar.
  render() {
    return (
      <div
        className="Tab"
        style={this.getStyle()}
        onClick={this.props.chooseTab.bind(this, this.props.singleTab.id)}
      >
        {this.props.singleTab.name}
      </div>
    );
  }
}

SingleTab.propTypes = {
  singleTab: PropTypes.object.isRequired
};

export default SingleTab;
