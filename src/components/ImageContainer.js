import React from "react";
import PropTypes from "prop-types";
import "./../Styles/ImageContainer.css";
// Contains and loads the image.
class ImageContainer extends React.Component {
  state = {
    currentImg: null,
    lastUrl: null
  };

  async fetchImageFromUrl() {
    try {
      // Fetch the svg-image and convert it to text.
      // The public url is added so that the project deploys correctly in subfolder
      const response = await fetch(
        `${process.env.PUBLIC_URL}` + this.props.url
      );
      const data = await response.text();

      // Changes the current image and saves it to state (by the url) so it is not needed to
      // fetch it again next time
      this.setState({
        [this.props.url]: data,
        currentImg: data,
        lastUrl: this.props.url
      });
    } catch (err) {
      console.log("Failed to get image, check your network connection");
    }
  }

  updateImage() {
    // Checks if the image is already in the state. If not, fetch it.
    if (this.state[this.props.url]) {
      // Changes current image to the image which is already loaded.
      this.setState({
        currentImg: this.state[this.props.url],
        lastUrl: this.props.url
      });
    } else {
      this.fetchImageFromUrl();
    }
  }
  // Called on initial load. Sets the initial state.
  componentDidMount() {
    this.updateImage();
  }

  componentDidUpdate() {
    // Check if the url has changed. If it has, update the image
    if (this.state.lastUrl !== this.props.url) {
      this.updateImage();
    }
  }
  // Adds the SVG content directly to the DOM
  render() {
    if (this.state.currentImg != null) {
      return (
        <div
          className="svg-image"
          dangerouslySetInnerHTML={{ __html: this.state.currentImg }}
        />
      );
    }
    return <div>loading...</div>;
  }
}

ImageContainer.propTypes = {
  url: PropTypes.string
};

export default ImageContainer;
