import React from "react";
import "./../Styles/Title.css";
//Simple header for the page
function Header() {
  return (
    <header>
      <h1 className="Header">The Gallery of Fine Arts</h1>
    </header>
  );
}

export default Header;
