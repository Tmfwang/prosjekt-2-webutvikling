import React from "react";
import PropTypes from "prop-types";
// Class that creates controls for the soundfiles
class SoundContainer extends React.Component {
  // We need to create a ref so that we can call the load method on the audio element
  constructor(props) {
    super(props);
    this.myRef = React.createRef();
  }
  // Method for updating the audiocontrols
  componentDidUpdate(prevProps) {
    if (prevProps.url !== this.props.url) {
      this.myRef.current.load();
    }
  }

  render() {
    return (
      <audio style={{ width: "100%" }} controls id="audio" ref={this.myRef}>
        {/* The public url is added so that the project deploys correctly in subfolder */}
        <source
          src={`${process.env.PUBLIC_URL}` + this.props.url}
          type={"audio/mpeg"}
        ></source>
      </audio>
    );
  }
}

SoundContainer.propTypes = {
  url: PropTypes.string.isRequired
};

export default SoundContainer;
