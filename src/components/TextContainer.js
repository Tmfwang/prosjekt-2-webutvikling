import React from "react";
import PropTypes from "prop-types";
// Loads the textfiles on the page.
class TextContainer extends React.Component {
  state = {
    currentText: null,
    lastUrl: null
  };

  async fetchTextFromUrl() {
    try {
      // Fetch the json and convert it to text.
      // The public url is added so that the project deploys correctly in subfolder
      const response = await fetch(
        `${process.env.PUBLIC_URL}` + this.props.url
      );
      const data = await response.json();

      // Changes the current text and saves it to state (by the url) so it is not needed to
      // fetch it again next time
      this.setState({
        [this.props.url]: data.text,
        currentText: data.text,
        lastUrl: this.props.url
      });
    } catch (err) {
      alert("Failed to get text, check your network connection");
    }
  }

  async updateText() {
    // Checks if the text is already in the state. If not, fetch it.
    if (this.state[this.props.url]) {
      // Changes current text to the text which is already loaded.
      this.setState({
        currentText: this.state[this.props.url],
        lastUrl: this.props.url
      });
    } else {
      this.fetchTextFromUrl();
    }
  }
  // Sets the initial state.
  componentDidMount() {
    this.updateText();
  }

  componentDidUpdate() {
    // Check if the url has changed. If it has, update the text
    if (this.state.lastUrl !== this.props.url) {
      this.updateText();
    }
  }

  render() {
    if (this.state.currentText != null) {
      return <h1>{this.state.currentText}</h1>;
    }
    return <div>loading...</div>;
  }
}

TextContainer.propTypes = {
  url: PropTypes.string
};

export default TextContainer;
