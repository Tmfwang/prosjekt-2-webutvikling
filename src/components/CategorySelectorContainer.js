import React from "react";
import CategorySelector from "./CategorySelector";
import PropTypes from "prop-types";

class CategorySelectorContainer extends React.Component {
  // Renders one CategorySelector-component for each element in the CategoryTypes-list. When one of
  // the CategorySelector-components change, the id of the CategoryType-object and its new
  // "chosen"-value is sendt to the component above with the categoryTypesChanged-function.
  render() {
    return this.props.categoryTypes.map(categoryType => (
      <CategorySelector
        key={categoryType.id}
        categoryType={categoryType}
        handleChange={changedTo => {
          this.props.categoryTypesChanged(categoryType.id, changedTo);
        }}
      />
    ));
  }
}

CategorySelectorContainer.propTypes = {
  categoryTypes: PropTypes.array.isRequired,
  categoryTypesChanged: PropTypes.func.isRequired
};

export default CategorySelectorContainer;
