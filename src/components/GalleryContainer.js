import React from "react";
import PropTypes from "prop-types";
import ImageContainer from "./ImageContainer";
import TextContainer from "./TextContainer";
import SoundContainer from "./SoundContainer";
import  "./../Styles/GalleryFlexStyle.css";

//GalleryContainer er komponenten som inneholder all media. Vi velger så å dele inn i egne komponenter for
//hver mediatype. Det er fortsatt praktisk å samle alle disse i en komponent.
class GalleryContainer extends React.Component {
  render() {
    return (
      <div className="ImageAndAudioContainer">
        <div className="ImageContainerDiv">
          <ImageContainer url={this.props.image} />
        </div>
        <div className="TextSoundContainer">
          <TextContainer url={this.props.text} />
          <SoundContainer url={this.props.sound} />
        </div>
      </div>
    );
  }
}
//Komponenten har tre props, som er URLene til de tre forskjellige typene media.
GalleryContainer.propTypes = {
  image: PropTypes.string.isRequired,
  text: PropTypes.string.isRequired,
  sound: PropTypes.string.isRequired
};

export default GalleryContainer;
