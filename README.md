# Prosjekt 2

Nettsiden er lastet opp på en virtuell maskin, og kan besøkes på følgende URL: http://it2810-33.idi.ntnu.no/prosjekt2/.

# Dokumentasjon

### React

Vi har laget nettsiden vår i React (Prosjekt opprettet med: Node.js -> npx create-react-app) og har en blanding mellom funksjonell og objektorientert struktur. For komponenter som har tilstand bruker vi objektorientert struktur,
som for eksempel for seleksjonsverktøy som kategorivelgeren og fanevelgeren. Andre komponenter som header og footer bruker funksjonell struktur, ettersom disse ikke trenger å ha en tilstand.

### Overordnet Komponentstruktur

![image](https://scontent-arn2-1.xx.fbcdn.net/v/t1.15752-9/70837413_503845563496856_250467988598161408_n.png?_nc_cat=110&_nc_eui2=AeG63NkoosxO-vb2r5py4zsFGAGs2ye9MxF-s-35JP0rba3u35lZfpi1UBThDG3HnfeisqrkwrBmU6uG7EqDLKHXBzhONP73-GgMAiqMcDZQWQ&_nc_oc=AQmhs7QNUaHlz_FgcbCQ0_wxEZ1CKr0OJpUhoSdbPAhSSL8zT6BzkCuAFYK7eEdcgeyCMxE-9GCrGIV3hlF-iFSf&_nc_ht=scontent-arn2-1.xx&oh=34872c084d4cac6c224529019c526225&oe=5E2F0C5A)

Strukturen er, som skissen viser, med en overordnet App komponent og et sett med containere i laget under, i tillegg til header og footer. TabContaineren vil alltid bestå av 4 tabs,
 og det vil alltid være 3 CategorySelector komponenter som hver består av 3 radiobuttons, disse er ikke med i skissen. GalleryContainer består av 3 subcontainere som er anvarlige
 for å loade mediatypene.

Vi har den overordnete tilstanden til nettsiden lagret i `App.js`, fordi vi syntes det var hensiktsmessig å ha tilstanden lagret sentralt i den overordnete komponenten slik at alle komponentene under kan aksessere alle tilstandene. Når en bruker endrer fane eller kategori vil den nye tilstanden sendes oppover i komponenthierarkiet til vi når toppen,
og tilstanden lagret i `App.js`vil oppdateres. Når dette skjer vil også tilstanden sendes ned (via props) til komponentene som laster media, slik at disse oppdateres i henhold til valgt fane og kategorier.

### HTML5 Web Storage

##### Local Storage

Vi bruker LocalStorage til å huske hvilken kombinasjon av fanevalg og kategorivalg brukeren sist hadde aktivt dersom man går inn på nettsiden etter å ha lukket den. Når
en bruker går inn på nettsiden etter å ha lukket den, eller refresher siden, vil ComponentDidMount metoden i App.js kalles, som sjekker om det finnes LocalStorage-verdier i brukerens nettleser, og benytter henholdsvis disse verdiene til å oppdatere tilstanden til komponentene lagret i `App.js`.
Dersom disse ikke finnes vil kategoriene velges tilfeldig, og fane 1 velges.

##### Session Storage

Vi bruker SessionStorage til å kunne gå tilbake til tidligere kombinasjoner av kategorier brukeren har valgt. Dette gjøres
ved at hver gang brukeren endrer kategorier vil nyeste kombinasjon pushes til en liste av kategorikombinasjoner i SessionStorage. Når en bruker trykker på
undo-knappen, popper vi den siste kombinasjonen fra listen, og oppdaterer tilstanden (og LocalStorage-variablene) deretter. Denne funksjonalitet benyttes ved å trykke på knappen som heter "Angre Kategorivalg".

### Bilder, Tekst og Lyd

Bildene og teksten lastes asynkront med bruk av AJAX. Komponentene som sørger for dette (ImageContainer og TextContainer) lagrer data som er lastet inn tidligere i komponentens tilstand, slik at det ikke er nødvendig å hente data på nytt dersom man har hentet den fra før. Data lagres i disse komponentene på formen `url: data`, slik at komponenten kan sjekke om den har hatt url-en tidligere. Tekstene er lagret i hver sin json-fil.

Lyden er direkte implementert med Audio-taggen til HTML5 og tilhørende kontrollelement. Når man skal endre lyden som skal spilles så blir kontrollene oppdatert til å matche
det nye lydklippet basert på tilstanden til AudioContainer komponenten. Det er ikke implementert noe caching av lyd ettersom dette ikke var et krav i oppgaven. Det ser imidlertidig ut som at lyden caches i Chrome (men ikke i Edge).

I den overordnede app-komponenten (App.js) ligger URL-ene til dataen lagret som strenger i et JSON-objekt. Disse URL-ene gis nedover hierarkiet til komponentene som skal laste mediaen. Disse benytter URLen og laster sin mediatype.

Alle bildene er med lisens til å bruke dem fritt til ikke-kommersiell bruk, i tillegg til at det ikke krav om navngiving av opphavsperson.

Lydklippene er hentet direkte fra [http://soundbible.com/](soundbible) under Attribution 3 [lisensen](https://creativecommons.org/licenses/by/3.0/).

### Krav til Funksjonalitet

Vi har en kombinasjon av faner rangert fra 1 til 4 og et sett med tre kategorier innen tekst, lyd og bilde. Basert på kombinasjonen av kategori og fane vil brukeren
få opp forskjellige kombinasjoner av disse tre typen media, og alt generers dynamisk umiddelbart når brukeren forandrer kombinasjonen av fane og kategori.
Dette oppfyller alle krav til funksjonalitet og genererer en fullverdig utstilling.

### Responsivt Design

Nettsiden følger oppgavens krav til responsivt design; bredden på alle sideelementene vil endres dynamisk relativt til enhetens bredde. Layoutet på nettsiden endres mellom breddeformat og høydeformat basert på bredden av vinduet. Dette er implementert med bruk av viewport som setter bredden av nettsiden lik enhetens bredde, og media-queries som endrer nettsidens CSS basert på den nåværende bredden. Mer spesifikt så brukes altså media-queries til å endre flexretningen mellom rad og kolonne til flexboxene som inneholder kategoribeholderen, bildebeholderen, tekstbeholderen og lydbeholderen. I tillegg brukes også media-queries til å endre plasseringen av fanebeholderen, slik at den er plassert øverst når man bruker en PC, og er plassert under kategoribeholderen når man bruker en mobil.

Nettsidens responsivitet er testet i forskjellige nettlesere, på en ordinær PC og på mobil i både portrett- og landskapsmodus.

### Testing

Vi har laget tre snapshot tester av forskjellige komponenter. Vi prøvde å lage snapshots av den helhetlige appen, men det ble fort problemer med noen av referansene vi brukte
nede i hierarkiet, samt at den opprinnelige staten i appen er tilfeldig, noe som gjør snapshottesting mer komplisert. I tillegg tar noen av komponentene inn metoder fra app-komponenten
som parametere, som gjør det mer komplisert å teste disse komponentene, spesielt også med tanke på at noen av dem benytter localstorage-verdier. Derfor innså vi at snapshot-tester lønner
seg bedre på komponenter som ikke skal endres hele tiden.

I tillegg har vi laget et par unit-tester til komponenten ImageContainer for å teste ut litt hvordan unit-tester fungerer i react. Her tester vi at komponenten henter data asynkront som den skal, ved å at vi setter opp en "dummy server" (med "nock") som vi spør etter data. I testen venter vi på at den asynkrone funksjonen er kjørt før vi går videre og sjekker at komponentens «state» har oppdatert seg riktig.

##### Hvordan Kjøre Testene
Skriv disse kommandoene i terminalen:

- npm install
- npm test